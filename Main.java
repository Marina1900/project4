package File;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Main {

    public static void main(String[] args) {
        String dName = "C://SE2020_LESSON9";
        File dir = new File(dName);
        boolean created1 = dir.mkdir();
        OutputStream output;
        int  numberOfDirs = (int) (1 + Math.random() * 3);
        System.out.println("Количество вложенных папок: " + numberOfDirs);
        for (int i = 0; i <= numberOfDirs; i++) {
            if (i != 0) {
                dName = dName.concat("//Directory_".concat(Integer.toString(i)));
            }
            dir = new File(dName);
            boolean created2 = dir.mkdir();
            int numberOfFiles = (int) (1 + Math.random() * 3);
            System.out.println("Количество файлов в "+ i + " папке: " + numberOfFiles);
            for (int j = 1; j <= numberOfFiles; j++) {
                try {
                    output = new FileOutputStream(dName.concat("//File_" + Integer.toString(j) + ".txt"));
                    int numberOfSymbols = (int) (10 + Math.random() * 191);
                    System.out.println("Количество символов в файле " + j + ": " + numberOfSymbols);
                    for (int k = 1; k <= numberOfSymbols; k++) {
                        int randomNum = (int) (Math.random() * 10);
                        char randomChar = (char) (randomNum + '0');
                        output.write(randomChar);
                        output.write(' ');
                    }
                    output.close();
                } catch (IOException e) {
                    System.out.println("Exception");
                }
            }
        }
    }
}